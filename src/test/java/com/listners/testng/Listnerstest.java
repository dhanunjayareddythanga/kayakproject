package com.listners.testng;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.functions.reusable.Reusablemethods;
import com.main.test.Browserlaunching;

public class Listnerstest extends Browserlaunching implements ITestListener {
	
	public void onTestStart(ITestResult Result) {
		
		
	}
	public void onTestSuccess(ITestResult Result) {
				
		    System.out.println("The name of the testcase success is :"+Result.getName());					
		
	}
	public void onTestFailure(ITestResult Result) {
		
		try {
			page.screenshot(Result.getName());
			} catch (Exception e) 
			{
			e.printStackTrace();
			} 
		System.out.println("the testcase "+Result.getName()+" is failed");
		
	}
	public void onTestSkipped(ITestResult Result) {
		
	}
	public void onTestFailedButWithinSuccessPercentage(ITestResult Result) {
		
	}
	public void onStart(ITestContext Result) {
	
		
	}
	public void onFinish(ITestContext Result) {
		
		
	}

}
