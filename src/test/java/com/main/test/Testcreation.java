package com.main.test;


import static org.testng.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.element.locators.OR;
import com.element.locators.Searchpage;
import com.functions.reusable.Reusablemethods;

@Listeners(com.listners.testng.Listnerstest.class)	
public class Testcreation extends Browserlaunching
{
	String url;
	String source_location;
	String destination_location;
	String boarding_date;
	String departure_date;
	
	@BeforeTest
	public void intialize() throws Exception
	{
		Properties pro=new Properties();
		FileInputStream fo =new FileInputStream("Months.properties");
		pro.load(fo);
		url=pro.getProperty("url");
		source_location=pro.getProperty("source_location1");
		destination_location=pro.getProperty("desination_location1");
		boarding_date=pro.getProperty("from_date1");
		departure_date=pro.getProperty("to_date1");
		
	}
	
	@BeforeMethod
	public void methidbefore()
	{
		page.cookiesdelete();
	}
	
	@Test
	public void test1() throws Exception
	{   
		page.openurl(url);
		page.sending(source_location,destination_location);
		page.selectdate(boarding_date);
		page.selectdate(departure_date);
		page.scroll();
		page.click(page.searchbutton);
		page.dialogboxhandle();
		page.waittillpresenceofelement(".prediction-info");
		//validation1
		page.elementpresent();
		//validaton2
		page.verify(page.sourceverification,page.desinationverification, source_location, destination_location);
		//validation3
		int noofsearchflights=page.searchedflights.size();
		if(noofsearchflights>0) 
		{
			System.out.println("no of flights are"+noofsearchflights);
		}
		else {
			Assert.fail("No results are displaying");
		}
		
		
	}
}
