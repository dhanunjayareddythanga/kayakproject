package com.functions.reusable;

import static org.testng.Assert.assertTrue;

import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.element.locators.OR;


public class Reusablemethods extends OR{
	

	public Reusablemethods(WebDriver d)
	{
		super(d);
	}
	public void openurl(String url)
	{
		d.manage().window().maximize();
		d.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
		d.get(url);
	}
	public void cookiesdelete()
	{
		d.manage().deleteAllCookies();
	}
	public void waittillpresenceofelement(String locator)
	{
		WebDriverWait wait=new WebDriverWait(d,60);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(locator)));
	}
	public static void screenshot(String sshotname) throws Exception
	{
		File fi=((TakesScreenshot)d).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(fi,new File("C:\\Users\\dhanunjayareddy.than\\Project Route\\Maven_Project\\screenshots"+sshotname+".jpg"));
	}
	public void click(WebElement a)
	{
		a.click();
	}
	public void quitdriver()
	{
		//d.quit();
	}
	public void sending(String fromdata,String todata) throws Exception
	{
		from.clear();
		from.sendKeys(fromdata);
		halting(2000);
		from.sendKeys(Keys.TAB);
		To.clear();
		To.sendKeys(todata);
		halting(2000);
		To.sendKeys(Keys.TAB);
	
	}
	public void dialogboxhandle()
	{
	
		try {
			Set<String> li=d.getWindowHandles();
			System.out.println(li.size());
			ArrayList<String> arr=new ArrayList<String>(li);
			d.switchTo().window(arr.get(1));
			if(d.findElement(By.xpath("//div[contains(@class,'flightsDriveBy fromBottom visible animate')]/div[@class='viewport']/div[@class='content']")).isDisplayed()) {
				WebElement dialog = d.findElement(By.xpath("//div[contains(@class,'flightsDriveBy fromBottom visible animate')]/div[@class='viewport']/div[@class='content']"));
				dialog.findElement(By.className("close")).click();;
				System.out.println("Handling popup successfully");
			}
		}
			
		catch(Exception e)
		{
			System.out.println("dialogbox is not showed");
		}
	}
	public void elementpresent()
	{
		verifytext.isDisplayed();
		String text=verifytext.getText();
		Assert.assertTrue(text.contains("Receive emails with price changes and travel"));
		System.out.println("validation 1 is success");
		
	}
	public void scroll() throws Exception
	{
		JavascriptExecutor jse = (JavascriptExecutor)d;
		jse.executeScript("window.scrollBy(0,-250)", "");
		Thread.sleep(4000);
	}
	
	public void verify(WebElement a,WebElement b,String source_location,String destination_location )
	{
		String verificationpoint1=a.getAttribute("value");
		System.out.println(verificationpoint1);
		String verificationpoint2=b.getAttribute("value");
		System.out.println(verificationpoint2);
		//Assert.assertTrue(verificationpoint1.contains(source_location)&&verificationpoint2.contains(destination_location),"its showing correct reasult");
		Assert.assertTrue(verificationpoint1.contains(source_location));
		Assert.assertTrue(verificationpoint2.contains(destination_location));
		System.out.println("validation 2 is success");
	}
	public void selectdate(String date) throws Exception
	{
		String[] datearray=date.split("-");
		String day=datearray[0];
		String month=datearray[1];
		String year=datearray[2];
		String monthAndyear=month+" "+year;
		findmonthAndYear(monthAndyear);
		String monthAndday=month+" "+day;
		Properties pro=new Properties();
		FileInputStream fi =new FileInputStream("Months.properties");
		pro.load(fi);
		String yearAndMonth=year+pro.getProperty(month);
		d.findElement(By.xpath("//div[contains(@id,'"+yearAndMonth+"')]/div[contains(@class,'month')]/div[@class='weeks']/div[@class='keel-grid weekGrid']/div[@aria-label='"+monthAndday+"']")).click();
		
		
	}
	public void findmonthAndYear(String monthAndyear) throws Exception
	{
		Thread.sleep(2000);
		try {
		if(d.findElement(By.xpath("//*[contains(text(),'" + monthAndyear + "')]")).isDisplayed())
			{
			System.out.println("month and year calendar is found");
			}
			else
			{
			nextbtn.click();
			Thread.sleep(2000);
			findmonthAndYear(monthAndyear);
			
			}
		}
		catch(NoSuchElementException e)
		{
			nextbtn.click();
			Thread.sleep(2000);
			findmonthAndYear(monthAndyear);
		}
	}
	public void halting(long millis)
	{
		try
		{
			Thread.sleep(millis);
		}
		catch(Exception e)
		{
			e.getStackTrace();
		}
	}
	
	
	

}

