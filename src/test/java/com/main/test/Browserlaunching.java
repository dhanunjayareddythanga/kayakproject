package com.main.test;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import com.functions.reusable.Reusablemethods;

public class Browserlaunching{
	public Reusablemethods page;
	@Parameters("browser")
	@BeforeTest
	public void set(String browser)
	{
	if(browser.equalsIgnoreCase("FF"))
	{
		page=PageFactory.initElements(new FirefoxDriver(),Reusablemethods.class);
	}
	else if(browser.equalsIgnoreCase("GC"))
	{
		System.setProperty("webdriver.chrome.driver","C:\\Users\\dhanunjayareddy.than\\Project Route\\chromedriver_win32\\chromedriver.exe");
		page=PageFactory.initElements(new ChromeDriver(),Reusablemethods.class);
	}
	else if(browser.equalsIgnoreCase("IE"))
	{
		System.setProperty("webdriver.ie.driver","C:\\Users\\dhanunjayareddy.than\\Project Route\\IEDriverServer_x64_3.8.0\\IEDriverServer.exe");
		page=PageFactory.initElements(new InternetExplorerDriver(),Reusablemethods.class);
	}
	}
@AfterTest
public void teardown()
{
	page.quitdriver();
}

}
