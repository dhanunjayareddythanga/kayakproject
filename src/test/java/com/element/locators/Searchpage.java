package com.element.locators;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Searchpage {
	@FindBy(xpath="//div[@class='Common-Booking-MultiBookProvider featured-provider cheapest Theme-button']/a[@class='booking-link']") public List<WebElement> viewdealbutton;
	@FindBy(xpath="//div[@class='col-info result-column']") public List<WebElement> searchedflights;
}
