package com.element.locators;

import java.io.FileInputStream;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OR {

public static   WebDriver d;

	public OR(WebDriver d)
	{
		this.d=d;
		PageFactory.initElements(d,this);
	}
	@FindBy(name="origin") public WebElement from;
	@FindBy(name="destination") public WebElement To;
	@FindBy(xpath="//div[contains(@id,'depart-input')") public WebElement fromDate;
	@FindBy(xpath="//div[contains(@id,'return-input')") public WebElement ToDate;
	@FindBy(xpath="//div[contains(@class,'centre')]/button[contains(@id,'submit')]") public WebElement searchbutton;
	@FindBy(xpath="//div[contains(@id,'\"+yearAndMonth+\"')]/div[contains(@class,'month')]/div[@class='weeks']/div[@class='keel-grid weekGrid']/div[@aria-label='\"+monthAndday+\"']") public static WebElement calendardatepic;
	@FindBy(xpath="//div[@class ='displayWrapper']/div[@class ='default-view']/div[@class ='navItem nextMonth']") public WebElement nextbtn;
	@FindBy(xpath="//div[contains(text(),'Receive emails')]") public WebElement verification_point;
	@FindBy(xpath="//div[contains(@class,'flightsDriveBy fromBottom visible animate')]/div[@class='viewport']/div[@class='content']/div[@class='close']") public WebElement dialogverfication;
	@FindBy(xpath="//div[@class='Common-Booking-MultiBookProvider featured-provider cheapest Theme-button']/a[@class='booking-link']") public List<WebElement> viewdealbutton;
	@FindBy(xpath="//div[contains(@class,'col-info result-column')]") public List<WebElement> searchedflights;
	@FindBy(name="origin") public  WebElement sourceverification;
	@FindBy(name="destination") public WebElement desinationverification;
	@FindBy(xpath="//div[contains(text(),'Receive emails with price changes and travel')]") public WebElement verifytext;

}
